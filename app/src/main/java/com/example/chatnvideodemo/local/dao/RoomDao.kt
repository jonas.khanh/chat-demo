package com.example.chatnvideodemo.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.chatnvideodemo.model.RoomLocal

@Dao
abstract class RoomDao {
    @Insert
    abstract suspend fun createRoom(room: RoomLocal)

    @Query("SELECT * FROM roomlocal")
    abstract suspend fun getAllRoom(): List<RoomLocal>
}