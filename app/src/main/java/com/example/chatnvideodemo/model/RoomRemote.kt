package com.example.chatnvideodemo.model

import com.google.gson.annotations.SerializedName

data class RoomRemote(
    @SerializedName("name") var name: String,
    @SerializedName("maxMember") var maxMember: Int)