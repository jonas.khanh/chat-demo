package com.example.chatnvideodemo.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RoomLocal(var name: String, var maxMember: Int){
    @PrimaryKey(autoGenerate = true) var id = 0
}